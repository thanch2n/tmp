## 蒲公英短信程序 及 web 控制界面

按主要功能范围，由四个用户运行一些程序：
| 用户     | 功能范围                                             |
|--------|--------------------------------------------------|
| smsmng | 发送、接收、重发及元数据服务                                   |
| smsmb  | 消息队列                                             |
| smsinf | 维护通道连接，包括cmpp，sgip客户端服务端实现                       |
| smsrun | 运行时发送、接收的客户端实现，相对于smsmng。另外由一些 php 维护一些中间状态和统计数据 |

| 角色  | 用户     | 进程       | 子进程                                                                                                                                                    | 作用                                                                         |
|-----|--------|----------|--------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------|
| web | smsmng | Seqctrl  | Seqsvr                                                                                                                                                 | Sequence control                                                           |
|     |        | Tcpctrl  | TcpAdapt SmsInf<br> TcpAdapt SmsOutf<br> TcpAdapt AcctInf<br> TcpAdapt TrxInf                                                                       | in<br> out<br> account<br> trans                                        |
|     |        | Autoctrl | AutoSender batSndSms zilmng<br> AutoSender RdsAg zilsms<br> AutoSender rjctWait zilsms<br>AutoSender syncFail zilsms<br>AutoSender dealFail zilsms | Batch send <br>redis again<br>reject wait<br>sync failed<br>deal failed |


